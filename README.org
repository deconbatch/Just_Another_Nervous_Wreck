* Just Another Nervous Wreck.
One of the creative coding example by deconbatch.
[[https://www.deconbatch.com/2020/01/just-another-nervous-wreck.html]['Just Another Nervous Wreck.' on Creative Coding : Examples with Code.]]

** Description of this code.
A creative coding made with Processing.

I had an idea that draws De Jong attractors on polar coordinates.
There is various way to draw on polar coordinates, and I choose this way.

    // De Jong Attractors
    float currX = sin(TWO_PI * djA * prevY) - cos(TWO_PI * djB * prevX);
    float currY = sin(TWO_PI * djC * prevX) - cos(TWO_PI * djD * prevY);

    // polar coordinates
    float dX = width  * 0.15 * currX;
    float dY = height * 0.15 * currX;
    float dR = rBase * currY;
    float px = dX * cos(dR) - dY * sin(dR);
    float py = dY * cos(dR) + dX * sin(dR);

There is a convergence problem in drawing attractors. I tried to avoid this by changing the attractor parameter in recurrence.

    // for avoid convergence (as much as possible)
    if (pCnt == floor(pMax * 0.6)){
      djA = random(-1.0, 1.0);
      djB = 1.0;
      djC = random(-1.0, 1.0);
      djD = 1.0;
    }

This code does not display any images on the screen but generates image files in frames directory.
You can make an animation with these files. 

** Change log.
   - created : 2020/01/26


